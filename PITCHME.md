# DAX
[YouTube](https://www.youtube.com/watch?v=1yWLhxYoq88)
[YouTube](https://www.youtube.com/watch?v=6ncHnWMEdic)
---
# relationship types in DAX
* knowing the direction of each relationship is critical
* all powerpivot relationships are **1 (dimension) to many (fact)**
  * powerBI (possibly powerquery/excel) can apply 
> bi-direcitonal cross filter direction = both to fix table aggregation from non-flat sources
* optimized for **star schema** 
* many to many can be performed though other tricks (patterns)
  * intermediate tables for multiple 1 to many relationships
  * can use calculated = dim tables as role-playing dims and relate them individually to fact tables
    * Equals
    * Distinct
    * Union
    * Intersect
    * NaturalInner/LeftOuterJoin
    * **Crossjoin**
    * Filter
    * SummarizeColumns
    * Summarize
    * Calendar
    * CalendarAuto
      * There's a table of column formulae @v=RiHpkN0gfPM
organize as:

Lookup/dimension tables on top| ex: Calendar, Customer, Product, Territories
---|---
Data/fact tables below| ex: Sales|

**YOU NEED A CALENDAR TBL TO AGGREGATE YTD/QTD/MTD**

---
# *evaluation contexts:*
every formula can have a different result based on context

---
# row context
**iterates rows**
* **cannot** create a filter context (but can use the calculate function to workaround)
* exists in a calculated column
  * also inside of special iterating functions like **filter** and **sumx/averagex/stdevx...** *table, row + row*
* doesnt follow relationship

---
# filter context - CALCULATE
**filters tables**
* can be altered with the **CALCULATE** function
* automatically follow relationships from 1 side --> many, just like the arrow indicates
* exist within pivot visuals
> **Calculate** ([any DAX expression], 
> filter 1, -> FILTER ( [ALL function implied if you don't specify here] Table , Field = "" )
> filter 2
> ...)
  * can add/modify/remove initial filters within the dimension called in the function
    * in fact, does not smart choose filters for you; you must specify or it assumes ALL
---
# context transition - create in-memory tables
* wrap calculate around rows; uniquely powerful way to execute from w/in a row context
```
calculate(
    sum(measure[])
)
```
* Calculate can also use OR conditions:

```
calculate(
    sum(measure[]),
    OR (
      measure[] = X,
      measure[] = Y)
    ),
    ALL (measure[])
  )
```

---
# bi-directional x-filtering
